<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\task;
use App\contacts;
use Illuminate\Http\Request;
//Route::group(['middleware' => ['web']], function () {

	Route::get('/', function () {
		$tasks = Task::orderBy('created_at','asc')->get();
		return view('tasks',['lists'=>$tasks]);
	    //return view('welcome');
	});

	/*Add new task*/
	Route::post('/task',function(Request $request){
		$validator = Validator::make($request->all(),[
			'name' => 'required|min:5|max:100',
		]);
		if($validator->fails()){
			return redirect('/')
				->withInput()
				->withErrors($validator);
		}

		$task = new Task;
		$task->name = $request->name;
		if($task->save()){
			$msg='successfully done';
			return redirect('/')
				->withInput()
				->withErrors($msg);
		}
		
	});

	/*Delete Task*/
	Route::delete('/task/{task}', function (Task $task) {
	    $task->delete();

	    return redirect('/');
	});


	/*new contact info*/
	Route::get('/contact',function(){
		$all_contacts = Contacts::orderBy('id','asc')->get();
		return view('contacts',['contacts'=>$all_contacts]);
	});

	Route::post('/addcontact',function(Request $request){
		//dd($request->all());

		$val = Validator::make($request->all(),[
			'name' => 'required|min:4|max:100',
			'contact' => 'required|min:11'
		]);
		if($val->fails()){
			return redirect('/contact')
				->withInput()
				->withErrors($val);
		}

		$contacts = new Contacts;
		$contacts->name = $request->name;
		$contacts->contact_no = $request->contact;

		if($contacts->save()){
			$msg='success';
			return redirect('/contact')
				->withInput()
				->withErrors($msg);
		}
	});
	/*new contact info*/

	/*remove contact*/
	Route::delete('/contacts/{id}',function(Contacts $id){
		$id->delete();
		return redirect('/contact');
	});
	/*remove contact*/
//});
