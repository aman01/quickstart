@extends('layouts.contactapp')
@section('content')
	<div class="panel-body">
		@include('common.errors')
		<form class="form-horizontal" method="POST" action="{{url('addcontact')}}">
			{{csrf_field()}}
			<div class="form-group col-md-6">
				<label class="control-label">Name</label>
				<input type="text" name="name" value="{{old('name')}}" class="form-control" placeholder="Ex: MD. AMANULLAH AMAN">
			</div>
			<div class="form-group col-md-6">
				<label class="control-label">Contact No</label>
				<input type="text" name="contact" value="{{old('contact')}}" class="form-control" placeholder="Ex: 01723779824">
			</div>
			<div class="row form-group col-md-12">
				<input type="submit" value="Save">
			</div>
		</form>
	</div>
	@if(count($contacts)>0)
		<table class="table table-striped task-table">
			<thead>
				<th>
					ID
				</th>
				<th>Name</th>
				<th>Contact No</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($contacts as $c)
				<tr>
					<td>{{$c->id}}</td>
					<td>{{$c->name}}</td>
					<td>{{$c->contact_no}}</td>
					<td>
						<form method="POST" action="{{url('contacts/'.$c->id)}}">
							{{ csrf_field() }}
         							{{ method_field('DELETE') }}
         						
	         						<button type="submit" class="btn btn-danger">
						                <i class="fa fa-trash"></i> Delete
						            </button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	@endif
@endsection