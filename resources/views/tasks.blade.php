@extends('layouts.app')
@section('content')
 	<!-- Bootstrap Boilerplate... -->
	<div class="panel-body">
		<!--Display validation error -->
		@include('common.errors')
		<!-- new task form-->
		<form action="{{url('task')}}" method="POST" class="form-horizontal">
			{{ csrf_field() }}

			<!--task name-->
			<div class="form-group">
				<label for="task" class="col-sm-3 control-label">Task</label>
				<div class="col-sm-6">
                    <input type="text" value="{{old('name')}}" name="name" id="task-name" class="form-control" placeholder="Write Name">
                </div>
			</div>

			 <!-- Add Task Button -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Add Task
                    </button>
                </div>
            </div>
		</form>
	</div>
	 <!-- TODO: Current Tasks -->
	 @if(count($lists)>0)
	 <div class="panel panel-default">
	 	<div class="panel-heading">
            Current Tasks
        </div>
         <div class="panel-body">
         	<table class="table table-striped task-table">
         		<thead>
         			<tr>
	         			<th>
	         				ID
	         			</th>
	         			<th>Name</th>
	         			<th>Delete</th>
         			</tr>
         		</thead>
         		<tbody>
         			<?php $no=1;?>
         			@foreach($lists as $l)
         				<tr>
         					<td>{{$no++}}</td>
         				
         					<td>{{$l->name}}</td>
         					<td>
         						<form action="{{ url('task/'.$l->id) }}" method="POST">
         							{{ csrf_field() }}
         							{{ method_field('DELETE') }}
         						
	         						<button type="submit" class="btn btn-danger">
						                <i class="fa fa-trash"></i> Delete
						            </button>
						        </form>
         					</td>
         				</tr>
         			@endforeach
         		</tbody>
         	</table>
         </div>
	 </div>
	 @endif
@endsection