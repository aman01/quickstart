@if(count($errors)>0)
	<div class="alert alert-danger">
		<!-- <strong>Whoops! Something went wrong!</strong> -->
    	@foreach($errors->all() as $e)
    		<label>{{$e}}</label>
    	@endforeach
	</div>
@endif
